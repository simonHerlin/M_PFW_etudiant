{-# LANGUAGE OverloadedStrings #-}

module View (mkpage, homeRoute, displayPoem, addPoem) where

import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Lucid

import qualified Model

myCss :: C.Css
myCss = do
    C.div C.# C.byClass "divCss" C.? do
        C.backgroundColor  C.beige
        C.border           C.solid (C.px 1) C.black
        C.margin           (C.em 1) (C.em 1) (C.em 1) (C.em 1)
        C.padding          (C.em 0) (C.em 1) (C.em 0) (C.em 1)
        C.width            (C.px 320)
        C.float            C.floatLeft
    C.div C.# C.byClass "divPoem" C.? do
      C.border           C.solid (C.px 1) C.black
      C.width            (C.px 320)
    C.p C.# C.byClass "pCss" C.?
        C.fontWeight C.bold


mkpage :: Lucid.Html () -> Lucid.Html () -> L.Text
mkpage titleStr page = renderText $ html_ $ do
  head_ $ do
    title_ titleStr
    style_ $ L.toStrict $ C.render myCss
  body_ page

poemFormat :: Model.Poem -> Lucid.Html()
poemFormat poem = div_ [class_ "divCss"] $ do
                    a_ [class_ "aCss", href_ (T.concat ["/display?id=", Model.getIdText poem])] $ do
                      h3_ (toHtml (Model.title poem))
                      p_  (toHtml (T.concat ["by ", Model.title poem, " (", Model.getYearText poem,")"]))

homeRoute :: [Model.Poem] -> Lucid.Html ()
homeRoute poems = do
  h1_ "Poem hub"
  a_ [href_ "/addPoem"] "write a new poem"
  div_ $ do
    toHtml $ mapM_ poemFormat poems
  
  
displayPoem :: Model.Poem -> Lucid.Html ()
displayPoem poem = do
  p_ [class_ "pCss"] (toHtml (Model.title poem))
  p_ (toHtml (T.concat ["by ", Model.author poem, " (", Model.getYearText poem,")"]))
  div_ [class_ "divPoem"] (toHtml (Model.body poem))
  a_ [href_ "/"] "Back Home"



addPoem :: Lucid.Html ()
addPoem = do
  h1_ "Write a new poem"
  with form_ [method_ "post", action_ "/add"] $ do
    div_ $ do
      label_ "Author"
      input_ [placeholder_ "Author", name_ "author"]
    div_ $ do
      label_ "Title"
      input_ [placeholder_ "Title", name_ "title"]
    div_ $ do
      label_ "Year"
      input_ [placeholder_ "Year", name_ "year"]
    div_ $ do
      label_ "Body"
      input_ [placeholder_ "Body", name_ "body"]
    with button_ [type_ "submit"] "Submit"
  with form_ [method_ "get", action_ "/"] $ do
    with button_ [type_ "submit"] "Cancel"
