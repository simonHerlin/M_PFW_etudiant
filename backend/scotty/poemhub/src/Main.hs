{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Trans (liftIO)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty (get, middleware, param, post, rescue, scotty, html, redirect)

import qualified Model
import qualified View

main = scotty 3000 $ do

  middleware logStdoutDev

  get "/" $ do
    poems <- liftIO Model.getAllPoems
    html $ View.mkpage "Poem hub - Home" $ View.homeRoute poems

  get "/display" $ do
    id <- param "id" `rescue` (\_ -> return (0::Int))
    poem <- liftIO (Model.getPoemById id)
    html $ View.mkpage "Poem hub - Read poem" $ View.displayPoem poem

  get "/addPoem" $ do
    html $ View.mkpage "Poem hub - Write a new poem" $ View.addPoem

  post "/add" $ do
    -- title <- param "title" `rescue` (\_ -> return (""::String))
    -- author <- param "author" `rescue` (\_ -> return (""::String))
    -- body <- param "body" `rescue` (\_ -> return (""::String))
    year <- param "year" `rescue` (\_ -> return (0::Int))
    -- Model.addPoem author title year body
    redirect "/"
