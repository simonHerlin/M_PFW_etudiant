{-# LANGUAGE OverloadedStrings #-}

module Model where

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL
import           Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

-- TODO implement SQL queries

dbName = "poemhub.db"

data Poem = Poem 
  { id :: Int
  , author :: T.Text
  , title :: T.Text
  , year :: Int
  , body :: T.Text 
  }

instance FromRow Poem where
  fromRow = Poem <$> field <*> field <*> field <*> field <*> field

getAllPoems :: IO [Poem]
getAllPoems = do
  conn <- SQL.open dbName
  peoms <- SQL.query_ conn "SELECT * FROM poems" :: IO [Poem]
  SQL.close conn
  return peoms
  
getPoemById :: Int -> IO Poem
getPoemById id = do
  conn <- SQL.open dbName
  poem <- SQL.query conn "SELECT * FROM poems WHERE id=?" (SQL.Only id) :: IO [Poem]
  SQL.close conn
  return (head poem)

getYearText :: Poem -> T.Text
getYearText = T.pack . show . Model.year

getIdText :: Poem -> T.Text
getIdText = T.pack . show . Model.id

-- addPoem:: String -> String -> Int -> String -> IO ()
-- addPoem auth title year body = do
--   conn <- SQL.open dbName
--   -- SQL.query conn "INSERT INTO poems VALUES("++ auth ++", "++ title ++", " ++ year ++ ", " ++ body ++ ")" --(auth, title, year, body)
--   SQL.close conn
  
-- https://framagit.org/nokomprendo/tuto_fonctionnel/blob/master/posts/tuto_fonctionnel_16/todolist/todolist0.hs