{-# LANGUAGE OverloadedStrings #-}
import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Lucid
import qualified Web.Scotty as S

divCss =
    C.div C.# C.byClass "divCss" C.? do
        C.backgroundColor  C.beige
        C.border           C.solid (C.px 1) C.black



main :: IO ()
main = S.scotty 3000 $ do
    S.get "/" $ S.html $ renderText 
        $ html_ $ do
            head_ $ 
                style_ $ L.toStrict $ C.render divCss
            body_ $ do
                h1_ "The helloscotty project"
                a_ [ href_ "hello" ] "go to hello page"

    S.get "/hello" $ do
        name <- S.param "name" `S.rescue` (\_ -> return "")
        S.html $ renderText $ html_ $ do
            head_ $ 
                style_ $ L.toStrict $ C.render divCss
            body_ $ do
                h1_ $ toHtml $ T.concat["Hello ", name]
                form_ [action_ "/hello/", method_ "get"] $ do
                    input_ [name_ "name", value_ "enter text here"]
                    input_ [type_ "submit", value_ "Valid"]
                a_ [ href_ "/" ] "go to home page"
