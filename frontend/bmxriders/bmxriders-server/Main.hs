{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson (ToJSON)
import Data.Maybe (fromMaybe)
import Data.Text.Lazy (isPrefixOf, Text, toLower)
import GHC.Generics (Generic)
import Network.Wai.Middleware.Cors (simpleCors)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import System.Environment (lookupEnv)
import Text.Read (readMaybe)
import Web.Scotty (get, json, middleware, param, redirect, scotty)

data Bmxrider = Bmxrider 
    { riderName  :: Text 
    , riderImg   :: Text 
    } deriving (Show, Generic)

instance ToJSON Bmxrider

bmxriders :: [Bmxrider]
bmxriders = 
    [ Bmxrider "Andy Buckworth" "andy-buckworth.jpg"
    , Bmxrider "Brandon Loupos" "brandon-loupos-1.jpg"
    , Bmxrider "Brandon Loupos" "brandon-loupos-2.jpg"
    , Bmxrider "Dave Mirra" "dave-mirra.jpg"
    , Bmxrider "Harry Main" "harry-main.jpg"
    , Bmxrider "Logan Martin" "logan-martin-1.jpg"
    , Bmxrider "Logan Martin" "logan-martin-2.png"
    , Bmxrider "Logan Martin" "logan-martin-3.jpg"
    , Bmxrider "Mark Webb" "mark-webb-1.jpg"
    , Bmxrider "Mark Webb" "mark-webb-2.jpg"
    , Bmxrider "Matt Hoffman" "matt-hoffman.jpg"
    , Bmxrider "Pat Casey" "pat-casey-1.jpg"
    , Bmxrider "Pat Casey" "pat-casey-2.jpg"
    ]

ridersFromName :: Text -> [Bmxrider]
ridersFromName name = filter (isPrefixOf name' . riderName') bmxriders
    where name' = toLower name
          riderName' = toLower . riderName

main :: IO ()
main = do
    portMayStr <- lookupEnv "PORT"
    let port = fromMaybe 3000 (portMayStr >>= readMaybe)
    scotty port $ do
        middleware logStdoutDev
        middleware simpleCors
        get "/api/bmxriders" $ json bmxriders
        get "/api/bmxriders/:name" $ param "name" >>= json . ridersFromName
        get "/" $ redirect "/index.html"
        middleware $ staticPolicy $ addBase "static"

