{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.IO as TIO
import qualified Data.Text as T
import           System.Environment (getArgs, getProgName)

-- main =  getArgs 
--     >>= readFile . head 
--     >>= (mapM_ putStrLn) . lines

main :: IO ()
main = do
    progName <- getProgName
    n <- getArgs
    if length n < 1 
    then do
        TIO.putStrLn $ T.concat ["usage: ", T.pack progName, " <infile>"]
        TIO.putStrLn $ T.concat ["e.g.: ", T.pack progName, " hpack.pnm"]
    else do
        file <- TIO.readFile $ head n
        mapM_ TIO.putStrLn (T.lines file)
        -- TIO.putStrLn file (works too)
