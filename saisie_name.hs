-- saisir::String -> IO String
-- saisir str = do
--     putStrLn $ " Entrer " ++ str ++ " ?"
--     getLine

-- main::IO()
-- main = do
--     nom <- saisir "nom"
--     prenom <- sasir "prenom"
--     putStrLn $ "Bonjour " ++ prenom ++ " " ++ nom ++ " !"

safeSqrt :: Double -> Maybe Double
safeSqrt x = if x > 0 then Just (sqrt x) else Nothing

safeMul2 :: Double -> Maybe Double
safeMul2 x = Just $ x * 2

main:: IO ()
main = do
    -- print $ safeSqrt (-8)
    -- print $ safeSqrt $ safeMul2 882
    print $ safeMul2 882 >>= safeSqrt
    print $ safeSqrt 882 >>= safeMul2

    print $ do
        x <- safeMul2 882
        y <- safeSqrt x
        return y