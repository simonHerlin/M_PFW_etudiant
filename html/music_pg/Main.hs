import qualified Data.Text as T
import qualified Database.PostgreSQL.Simple as SQL
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)

data Name = Name T.Text deriving Show

instance FromRow Name where
    fromRow = Name <$> field 

main :: IO ()
main = do
  conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=mybd user=toto password='toto'"
  putStrLn "\n*** id et nom des artistes ***"
  res <- SQL.query_ conn "SELECT * FROM artists" :: IO [(Int,T.Text)]
  mapM print res

  putStrLn "\n*** id et nom de l'artiste 'Radiohead' ***"
  res <- SQL.query conn "SELECT * FROM artists a WHERE a.name = (?)" (SQL.Only $ T.pack "Radiohead") :: IO [(Int,T.Text)]
  mapM print res

  putStrLn "\n*** tous les champs des titres dont le nom contient 'ust' et dont l'id > 1 ***"
  res <- SQL.query_ conn "SELECT * FROM titles t WHERE t.name LIKE '%ust%' AND t.id > 1" :: IO [(Int, Int, T.Text)]
  mapM print res

  putStrLn "\n*** noms des titres (en utilisant le type Name) ***"
  res <- SQL.query_ conn "SELECT t.name FROM titles t" :: IO [Name]
  mapM print res

  putStrLn "\n*** noms des titres (en utilisant une liste de T.Text) ***"
  res <- SQL.query_ conn "SELECT t.name FROM titles t" :: IO [[T.Text]] 
  mapM print res

  SQL.close conn