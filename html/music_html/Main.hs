{-# LANGUAGE OverloadedStrings #-}


import qualified Database.PostgreSQL.Simple as SQL
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)
import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy as L
-- import           Lucid
import           System.Environment (getArgs, getProgName)
import Lucid

data Music = Music
  { _title :: T.Text,
    _artist :: T.Text
  } deriving (Show)

instance FromRow Music where
  fromRow = Music <$> field <*> field

-- myCss :: C.Css
-- myCss = C.div C.# C.byClass "myCss" C.? do
--   C.backgroundColor  C.beige
--   C.border           C.solid (C.px 1) C.black

musicToString:: Music -> String
musicToString (Music _title _artist) = show _title ++ " - " ++ show _artist 

myPage :: [Music] -> Html ()
myPage musics = do
    doctype_
    html_ $ do
      head_ $ do
        meta_ [charset_ "utf-8"]
      body_ $ do
        h1_ "My list"
        div_ $ ul_ $ mapM_ (li_ . toHtml . musicToString) musics


main :: IO ()
main = do
  conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=mybd user=toto password='toto'"
  res <- SQL.query_ conn "SELECT t.name, a.name FROM artists a INNER JOIN titles t ON t.artist = a.id" :: IO[Music]
  -- mapM musicToString res
  renderToFile "test.html" $ myPage res
  SQL.close conn